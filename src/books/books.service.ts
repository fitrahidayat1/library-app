import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BooksEntity } from './books.entity';
import { BookDto } from './dtos/create.book.dto';

@Injectable()
export class BooksService {
  constructor(
    @InjectRepository(BooksEntity)
    private booksRepository: Repository<BooksEntity>,
  ) {}
  async getBooks() {
    const books = await this.booksRepository.find();
    return books;
  }

  async findOne(id: number): Promise<BooksEntity> {
    return this.booksRepository.findOne(id);
  }

  async create(bookData: BookDto) {
    const bookEntity = this.booksRepository.create(bookData);
    return this.booksRepository.save(bookEntity);
  }

  async update(id: number, bookData: Partial<BookDto>) {
    await this.booksRepository.update({ id }, bookData);
    return await this.booksRepository.findOne({ id });
  }

  async delete(id: number) {
    await this.booksRepository.delete(id);
    return { deleted: true };
  }
}
