import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { BooksEntity } from './books.entity';
import { BooksService } from './books.service';
import { BookDto } from './dtos/create.book.dto';

@Controller('v1/books')
export class BooksController {
  constructor(private bookService: BooksService) {}

  @Get()
  public async getBooks(): Promise<BooksEntity[]> {
    const books = await this.bookService.getBooks();
    return books;
  }

  @Get(':id')
  async getOneBook(@Param('id') id: number) {
    const data = this.bookService.findOne(id);
    return {
      message: 'Book fetched successfully',
      data,
    };
  }

  @Post()
  async createBook(@Body() bodyData: BookDto): Promise<BooksEntity> {
    return this.bookService.create(bodyData);
  }

  @Put(':id')
  async updateBook(
    @Param('id') id: number,
    @Body() bookData: Partial<BookDto>,
  ) {
    await this.bookService.update(id, bookData);
    return this.bookService.findOne(id);
  }

  @Delete(':id')
  async deleteBook(@Param('id') id: number) {
    await this.bookService.delete(id);
    return {
      message: 'Book deleted Successfully',
    };
  }
}
